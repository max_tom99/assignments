﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMove2 : MonoBehaviour
{
    public float speed_variable = 1;
    public float speed = 0.01f;
    public float frames = 0;
    // Update is called once per frame
    void Update()
    {
        var pos = transform.position;
        if (frames >= (0.005 * speed_variable))
        {
            transform.position = new Vector2(pos.x, pos.y + (speed * speed_variable));
            if (pos.y > 4.5)
            {
                speed = -0.01f;
            }
            if (pos.y < -4.5)
            {
                speed = 0.01f;
            }
            //safe zone at the start
            if (pos.x < -2.75)
            {
                if (pos.y > 2.5)
                {
                    speed = -0.01f;
                }
                if (pos.y < -4.5)
                {
                    speed = 0.01f;
                }
            }
            frames = 0;
        }
        frames += (Time.deltaTime * speed_variable);
    }
}
