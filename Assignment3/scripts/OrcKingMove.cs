﻿using System.Collections;
using System.Collections.Generic;
using System.Xml.Serialization;
using UnityEngine;

public class OrcKingMove : MonoBehaviour
{
    public float speed_variable = 1;
    public float speed = 0.01f;
    public float frames = 0;
    public int direction = 0;
    public float exitx
    {
        get
        {
            return x.doorx;
        }
    }
    public float exity
    {
        get
        {
            return x.doory;
        }
    }
    private EndTexture x;
    private void Start()
    {
        x = GameObject.Find("End").GetComponent<EndTexture>();

    }
    void Update()
    {
        var pos = transform.position;
        if (frames >= (0.005 * speed_variable))
        {
            if (direction == 0)
            {
                transform.position = new Vector2(pos.x + (speed * speed_variable), pos.y);
                if (pos.x >= (exitx + 2.5))
                {
                    direction = 1;
                }
            }
            if (direction == 1)
            {
                transform.position = new Vector2(pos.x , pos.y + (speed * speed_variable));
                if (pos.y >= (exity + 2.5))
                {
                    direction = 2;
                }
            }
            if (direction == 2)
            {
                transform.position = new Vector2(pos.x - (speed * speed_variable), pos.y);
                if (pos.x <= (exitx - 0.5))
                {
                    direction = 3;
                }
            }
            if (direction == 3)
            {
                transform.position = new Vector2(pos.x , pos.y - (speed * speed_variable));
                if (pos.y <= (exity - 0.5))
                {
                    direction = 0;
                }
            }

            frames = 0;
        }
        frames += (Time.deltaTime * speed_variable);
    }
}

