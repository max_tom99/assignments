﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthUpdater : MonoBehaviour
{
    public int health
    {
        get
        {
            return h.health;
        }
    }
    private PlayerCollisions h;

    private void Update()
    {
        h = GameObject.Find("Player").GetComponent<PlayerCollisions>();
        Text theText;
        theText = gameObject.GetComponent<Text>();
        theText.text = ("Health: " + health);
    }
}
