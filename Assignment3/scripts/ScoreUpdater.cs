﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreUpdater : MonoBehaviour
{
    public int coins
    {
        get
        {
            return c.coins;
        }
    }
    private PlayerCollisions c;

    private void Update()
    {
        c = GameObject.Find("Player").GetComponent<PlayerCollisions>();
        Text theText;
        theText = gameObject.GetComponent<Text>();
        theText.text = ("Coins:"+ coins);
    }
}
