﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OrcTextures : MonoBehaviour
{
    public Sprite[] player;
    private int Current_Index = 0;
    public float frames = 0;
    public float speed
    {
        get
        {
            return sm.speed;
        }
    }
    public float speed_variable
    {
        get
        {
            return sv.speed_variable;
        }
    }
    private EnemyMove sm;
    private EnemyMove sv;
    // Start is called before the first frame update
    void Awake()
    {
        player = Resources.LoadAll<Sprite>("Art/orc");
        sm = gameObject.GetComponent<EnemyMove>();
        sv = gameObject.GetComponent<EnemyMove>();
        Current_Index = 92;
        gameObject.GetComponent<SpriteRenderer>().sprite = player[Current_Index];
    }

    // Update is called once per frame
    void Update()
    {
        if (frames >= (0.05 / speed_variable))
        {
            if (speed < 0)
            {
                if ((Current_Index <= 74) || (Current_Index >= 82))
                {
                    Current_Index = 74;
                }
                gameObject.GetComponent<SpriteRenderer>().sprite = player[Current_Index];
                Current_Index += 1;

            }

            if (speed > 0)
            {
                if ((Current_Index <= 92) || (Current_Index >= 100))
                {
                    Current_Index = 92;
                }
                gameObject.GetComponent<SpriteRenderer>().sprite = player[Current_Index];
                Current_Index += 1;
            }
            frames = 0;
        }
        frames += Time.deltaTime;
    }
}
