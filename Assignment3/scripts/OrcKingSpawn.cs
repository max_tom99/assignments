﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OrcKingSpawn : MonoBehaviour
{
    public float exitx
    {
        get
        {
            return x.doorx;
        }
    }
    public float exity
    {
        get
        {
            return x.doory;
        }
    }
    private EndTexture x;

    private void Start()
    {
        x = GameObject.Find("End").GetComponent<EndTexture>();
        transform.position = new Vector3(exitx , exity - 0.5f, -1);
    }
}

