﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;
using UnityEngine.UIElements;

public class GroundTexture : MonoBehaviour
{
    public int random;
    GameObject theTile;
    private GameObject tile0;
    private GameObject tile1;
    private GameObject tile2;
    private GameObject tile3;
    private GameObject tile4;
    private GameObject tile5;
    private GameObject tile6;
    private GameObject tile7;
    private void Awake()
    {
        tile0 = (GameObject)Resources.Load("Prefabs/Medieval_props_free_0");
        tile1 = (GameObject)Resources.Load("Prefabs/Medieval_props_free_1");
        tile2 = (GameObject)Resources.Load("Prefabs/Medieval_props_free_2");
        tile3 = (GameObject)Resources.Load("Prefabs/Medieval_props_free_3");
        tile4 = (GameObject)Resources.Load("Prefabs/Medieval_props_free_4");
        tile5 = (GameObject)Resources.Load("Prefabs/Medieval_props_free_5");
        tile6 = (GameObject)Resources.Load("Prefabs/Medieval_props_free_6");
        tile7 = (GameObject)Resources.Load("Prefabs/Medieval_props_free_7");

    }

    private void Start()
    {
        for (float yd = -4.5f; yd <= 4.5f; yd++)
            for (float xd = -4.5f; xd <= 4.5f; xd++)
            {
                random = Random.Range(0, 10);
                if (random == 0)
                {
                    theTile = (GameObject)Instantiate(tile0);
                    theTile.layer = 0;
                }
                if (random == 1)
                {
                    theTile = (GameObject)Instantiate(tile1);
                    theTile.layer = 0;
                }
                if (random == 2)
                {
                    theTile = (GameObject)Instantiate(tile2);
                    theTile.layer = 0;
                }
                if (random == 3)
                {
                    theTile = (GameObject)Instantiate(tile3);
                    theTile.layer = 0;
                }
                if (random == 4)
                {
                    theTile = (GameObject)Instantiate(tile4);
                    theTile.layer = 0;
                }
                if (random == 5)
                {
                    theTile = (GameObject)Instantiate(tile5);
                    theTile.layer = 0;
                }
                if (random == 6)
                {
                    theTile = (GameObject)Instantiate(tile0);
                    theTile.layer = 0;
                }
                if (random == 7)
                {
                    theTile = (GameObject)Instantiate(tile7);
                    theTile.layer = 0;
                }
                if (random == 8)
                {
                    theTile = (GameObject)Instantiate(tile0);
                    theTile.layer = 0;
                }
                if (random == 9)
                {
                    theTile = (GameObject)Instantiate(tile0);
                    theTile.layer = 0;
                }
                theTile.transform.position = new Vector2(xd, yd);
            }
    }
}
