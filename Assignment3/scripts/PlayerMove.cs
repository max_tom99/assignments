﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove : MonoBehaviour
{
    public float speed = 0.01f;
    public float speed_variable = 1;
    public float frames = 0;
    public int game_over
    {
        get
        {
            return coll.game_over;
        }
    }

    private PlayerCollisions coll;
    private void Awake()
    {
        coll = gameObject.GetComponent<PlayerCollisions>();
    }
    // Update is called once per frame
    void Update()
    {
        if (game_over == 0)
        {
            if (frames >= (0.005 * speed_variable)){
                var pos = transform.position;
                if (Input.anyKey)
                {
                    if (pos.y < 4.5)
                    {
                        if (Input.GetKey("up"))
                        {
                            transform.position = new Vector2(pos.x, pos.y + (speed * speed_variable));
                            if (pos.x > -4.75)
                            {
                                if (Input.GetKey("left"))
                                {
                                    transform.position = new Vector2(pos.x - (speed * speed_variable), pos.y + (speed * speed_variable));
                                }
                            }
                            if (pos.x < 4.75)
                            {
                                if (Input.GetKey("right"))
                                {
                                    transform.position = new Vector2(pos.x + (speed * speed_variable), pos.y + (speed * speed_variable));
                                }
                            }
                        }

                    }
                    if (pos.y > -4.5)
                    {
                        if (Input.GetKey("down"))
                        {
                            transform.position = new Vector2(pos.x, pos.y - (speed * speed_variable));
                            if (pos.x > -4.75)
                            {
                                if (Input.GetKey("left"))
                                {
                                    transform.position = new Vector2(pos.x - (speed * speed_variable), pos.y - (speed * speed_variable));
                                }
                            }
                            if (pos.x < 4.75)
                            {
                                if (Input.GetKey("right"))
                                {
                                    transform.position = new Vector2(pos.x + (speed * speed_variable), pos.y - (speed * speed_variable));
                                }
                            }
                        }
                    }
                    if (pos.x > -4.75)
                    {
                        if (Input.GetKey("left"))
                        {
                            transform.position = new Vector2(pos.x - (speed * speed_variable), pos.y);
                            if (pos.y < 4.5)
                            {
                                if (Input.GetKey("up"))
                                {
                                    transform.position = new Vector3(pos.x - (speed * speed_variable), pos.y + (speed * speed_variable));
                                }
                            }
                            if (pos.y > -4.5)
                            {
                                if (Input.GetKey("down"))
                                {
                                    transform.position = new Vector2(pos.x - (speed * speed_variable), pos.y - (speed * speed_variable));
                                }
                            }
                        }
                    }
                    if (pos.x < 4.75)
                    {
                        if (Input.GetKey("right"))
                        {
                            transform.position = new Vector2(pos.x + (speed * speed_variable), pos.y);
                            if (pos.y < 4.5)
                            {
                                if (Input.GetKey("up"))
                                {
                                    transform.position = new Vector2(pos.x + (speed * speed_variable), pos.y + (speed * speed_variable));
                                }
                            }
                            if (pos.y > -4.5)
                            {
                                if (Input.GetKey("down"))
                                {
                                    transform.position = new Vector2(pos.x + (speed * speed_variable), pos.y - (speed * speed_variable));
                                }
                            }
                        }
                    }
                }
                frames = 0;
            }
            frames += (Time.deltaTime * speed_variable);
        }
    }
}
