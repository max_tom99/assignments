﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkeletonTexture2 : MonoBehaviour
{
    public Sprite[] player;
    private int Current_Index = 0;
    public float frames = 0;
    public float speed
    {
        get
        {
            return sm.speed;
        }
    }
    public float speed_variable
    {
        get
        {
            return sv.speed_variable;
        }
    }
    private EnemyMove2 sm;
    private EnemyMove2 sv;
    // Start is called before the first frame update
    void Awake()
    {
        player = Resources.LoadAll<Sprite>("Art/skeleton");
        sm = gameObject.GetComponent<EnemyMove2>();
        sv = gameObject.GetComponent<EnemyMove2>();
        Current_Index = 60;
        gameObject.GetComponent<SpriteRenderer>().sprite = player[Current_Index];
    }

    // Update is called once per frame
    void Update()
    {
        if (frames >= (0.05 / speed_variable))
        {
            if (speed > 0)
            {
                if ((Current_Index <= 60) || (Current_Index >= 68))
                {
                    Current_Index = 60;
                }
                gameObject.GetComponent<SpriteRenderer>().sprite = player[Current_Index];
                Current_Index += 1;

            }

            if (speed < 0)
            {
                if ((Current_Index <= 78) || (Current_Index >= 86))
                {
                    Current_Index = 78;
                }
                gameObject.GetComponent<SpriteRenderer>().sprite = player[Current_Index];
                Current_Index += 1;
            }
            frames = 0;
        }
        frames += Time.deltaTime;
    }
}
