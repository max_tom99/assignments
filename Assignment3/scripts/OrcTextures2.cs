﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OrcTextures2 : MonoBehaviour
{
    public Sprite[] player;
    private int Current_Index = 0;
    public float frames = 0;
    public float speed
    {
        get
        {
            return sm.speed;
        }
    }
    public float speed_variable
    {
        get
        {
            return sv.speed_variable;
        }
    }
    private EnemyMove2 sm;
    private EnemyMove2 sv;
    // Start is called before the first frame update
    void Awake()
    {
        player = Resources.LoadAll<Sprite>("Art/orc");
        sm = gameObject.GetComponent<EnemyMove2>();
        sv = gameObject.GetComponent<EnemyMove2>();
        Current_Index = 65;
        gameObject.GetComponent<SpriteRenderer>().sprite = player[Current_Index];
    }

    // Update is called once per frame
    void Update()
    {
        if (frames >= (0.05 / speed_variable))
        {
            if (speed > 0)
            {
                if ((Current_Index <= 65) || (Current_Index >= 73))
                {
                    Current_Index = 65;
                }
                gameObject.GetComponent<SpriteRenderer>().sprite = player[Current_Index];
                Current_Index += 1;

            }

            if (speed < 0)
            {
                if ((Current_Index <= 83) || (Current_Index >= 91))
                {
                    Current_Index = 83;
                }
                gameObject.GetComponent<SpriteRenderer>().sprite = player[Current_Index];
                Current_Index += 1;
            }
            frames = 0;
        }
        frames += Time.deltaTime;
    }
}
