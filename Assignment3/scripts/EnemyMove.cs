﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class EnemyMove : MonoBehaviour
{
    public float speed_variable = 1;
    public float speed = 0.01f;
    public float frames = 0;
    // Update is called once per frame
    void Update()
    {            
        var pos = transform.position;
        if (frames >= (0.005 * speed_variable))
        {
            transform.position = new Vector2(pos.x + (speed * speed_variable), pos.y);
            if (pos.x > 4.75)
            {
                speed = -0.01f;
            }
            if (pos.x < -4.75)
            {
                speed = 0.01f;
            }
            //safe zone at the start
            if (pos.y > 2.5)
            {
                if (pos.x > 4.75)
                {
                    speed = -0.01f;
                }
                if (pos.x < -2.75)
                {
                    speed = 0.01f;
                }
            }
            frames = 0;
        }
        frames += (Time.deltaTime *speed_variable);
    }
}
