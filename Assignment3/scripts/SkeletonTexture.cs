﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkeletonTexture : MonoBehaviour
{
    public Sprite[] player;
    private int Current_Index = 0;
    public int frames = 0;
    public float speed
    {
        get
        {
            return sm.speed;
        }
    }
    public float speed_variable
    {
        get
        {
            return sv.speed_variable;
        }
    }
    private EnemyMove sm;
    private EnemyMove sv;
    // Start is called before the first frame update
    void Awake()
    {
        player = Resources.LoadAll<Sprite>("Art/skeleton");
        sm = gameObject.GetComponent<EnemyMove>();
        sv = gameObject.GetComponent<EnemyMove>();
        Current_Index = 87;
        gameObject.GetComponent<SpriteRenderer>().sprite = player[Current_Index];
    }

    // Update is called once per frame
    void Update()
    {
        if (frames >= (15/ speed_variable))
        {
            if (speed < 0)
            {
                if ((Current_Index <= 69) || (Current_Index >= 77))
                {
                    Current_Index = 69;
                }
                gameObject.GetComponent<SpriteRenderer>().sprite = player[Current_Index];
                Current_Index += 1;

            }

            if (speed > 0)
            {
                if ((Current_Index <= 87) || (Current_Index >= 95))
                {
                    Current_Index = 87;
                }
                gameObject.GetComponent<SpriteRenderer>().sprite = player[Current_Index];
                Current_Index += 1;
            }
        frames = 0;
        }           
        frames += 1;
    }
}
