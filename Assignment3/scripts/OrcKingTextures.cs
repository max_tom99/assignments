﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OrcKingTextures : MonoBehaviour
{
    public Sprite[] player;
    private int Current_Index = 0;
    public float frames = 0;
    public float direction
    {
        get
        {
            return sm.direction;
        }
    }
    public float speed_variable
    {
        get
        {
            return sm.speed_variable;
        }
    }
    private OrcKingMove sm;
    // Start is called before the first frame update
    void Awake()
    {
        player = Resources.LoadAll<Sprite>("Art/orc king");
        sm = gameObject.GetComponent<OrcKingMove>();
        Current_Index = 60;
        gameObject.GetComponent<SpriteRenderer>().sprite = player[Current_Index];
    }

    // Update is called once per frame
    void Update()
    {
        if (frames >= (0.05 / speed_variable))
        {
                if (direction == 3)
                {
                    if ((Current_Index <= 78) || (Current_Index >= 86))
                    {
                        Current_Index = 78;
                    }
                    gameObject.GetComponent<SpriteRenderer>().sprite = player[Current_Index];
                    Current_Index += 1;

                }

                if (direction == 1)
                {
                    if ((Current_Index <= 60) || (Current_Index >= 68))
                    {
                        Current_Index = 60;
                    }
                    gameObject.GetComponent<SpriteRenderer>().sprite = player[Current_Index];
                    Current_Index += 1;
                }
                if (direction == 2)
                {
                    if ((Current_Index <= 69) || (Current_Index >= 77))
                    {
                        Current_Index = 69;
                    }
                    gameObject.GetComponent<SpriteRenderer>().sprite = player[Current_Index];
                    Current_Index += 1;

                }

                if (direction == 0)
                {
                    if ((Current_Index <= 87) || (Current_Index >= 95))
                    {
                        Current_Index = 87;
                    }
                    gameObject.GetComponent<SpriteRenderer>().sprite = player[Current_Index];
                    Current_Index += 1;
                }
            frames = 0;
        }
        frames += Time.deltaTime;
    }
}

