﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExitDoorTexture : MonoBehaviour
{

    public Sprite[] open_door;
    public int game_win = 0;
    public int dooropen
    {
        get
        {
            return door.door_open;
        }
    }
    public float exitx
    {
        get
        {
            return x.doorx;
        }
    }
    public float exity
    {
        get
        {
            return x.doory;
        }
    }
    private PlayerCollisions door;
    private EndTexture x;
    private void Start()
    {
        door = GameObject.Find("Player").GetComponent<PlayerCollisions>();
        x = GameObject.Find("End").GetComponent<EndTexture>();
        open_door = Resources.LoadAll<Sprite>("Medieval_props_free");
        transform.position = new Vector3(exitx + 1, exity + 1, 0);
    }
    private void Update()
    {
        if (dooropen == 1)
        {
                gameObject.GetComponent<SpriteRenderer>().sprite = open_door[2];
            game_win = 1;
        }
    }
}
