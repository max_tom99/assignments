﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartTexture : MonoBehaviour
{
    GameObject theTile;
    private GameObject tile;
    private void Awake()
    {
        tile = (GameObject)Resources.Load("Prefabs/Medieval_props_free_8");
    }

    private void Start()
    {
        for (float yd = 3.5f; yd <= 4.5f; yd++)
            for (float xd = -4.5f; xd <= -3.5f; xd++)
            {
                theTile = (GameObject)Instantiate(tile);
                theTile.transform.position = new Vector3(xd, yd, -1);
                theTile.transform.localScale = new Vector3(2.1f, 2.1f, 2.1f);
            }
    }
}
