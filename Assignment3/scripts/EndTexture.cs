﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndTexture : MonoBehaviour
{
    GameObject theTile;
    private GameObject tile;
    public int random1;
    public int random2;
    public int doorx;
    public int doory;
    private void Awake()
    {
        tile = (GameObject)Resources.Load("Prefabs/Medieval_props_free_8");
        random1 = Random.Range(-4, 2);
        random2 = Random.Range(-4, 2);

        if ((random1 <= 0) && (random2 >= 0 ))
        {
            random1 = Random.Range(0, 4);
        }
        doorx = random1;
        doory = random2;

        theTile = (GameObject)Instantiate(tile);
        theTile.transform.position = new Vector3(doorx + 0.5f, doory + 0.5f, -1);
        theTile.transform.localScale = new Vector3(2.1f, 2.1f, 2.1f);

        theTile = (GameObject)Instantiate(tile);
        theTile.transform.position = new Vector3(doorx + 1.5f, doory + 0.5f, -1);
        theTile.transform.localScale = new Vector3(2.1f, 2.1f, 2.1f);
        theTile = (GameObject)Instantiate(tile);
        theTile.transform.position = new Vector3(doorx + 0.5f, doory + 1.5f, -1);
        theTile.transform.localScale = new Vector3(2.1f, 2.1f, 2.1f);
        theTile = (GameObject)Instantiate(tile);
        theTile.transform.position = new Vector3(doorx + 1.5f, doory + 1.5f, -1);
        theTile.transform.localScale = new Vector3(2.1f, 2.1f, 2.1f);

    }
}
