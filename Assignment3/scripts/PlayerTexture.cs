﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerTexture : MonoBehaviour
{
    public Sprite[] player;
    private int Current_Index = 0;
    public float frames = 0;
    public Color normal;
    public Color fade_out;
    public float fade_speed;
    public int flash = 0;
    public int game_over
    {
        get
        {
            return coll.game_over;
        }
    }
    public int hurt
    {
        get
        {
            return coll.hurt;
        }
    }
    public int game_win
    {
        get
        {
            return win.game_win;
        }
    }
    public float speed_variable
    {
        get
        {
            return sv.speed_variable;
        }
    }
    private PlayerMove sv;
    private PlayerCollisions coll;
    private ExitDoorTexture win;
    public float Fade = 1f;
    private void Awake()
    {
        coll = gameObject.GetComponent<PlayerCollisions>();
        win = GameObject.Find("Exit").GetComponent<ExitDoorTexture>();
        sv = gameObject.GetComponent<PlayerMove>();
    }
    // Start is called before the first frame update
    private void Start()
    {
        player = Resources.LoadAll<Sprite>("Art/player");
        Current_Index = 18;
        gameObject.GetComponent<SpriteRenderer>().sprite = player[Current_Index];
        normal = gameObject.GetComponent<SpriteRenderer>().material.color;
    }

    // Update is called once per frame
    void Update()
    {
        if (game_win == 0)
        {
            if (game_over == 0)
            {
                if (frames >= (0.05/ speed_variable))
                {
                    if (Input.anyKey)
                    {
                        if (((Input.GetKey("up")) && !(Input.GetKey("left"))) && !(Input.GetKey("right")))
                        {
                            if (Current_Index >= 8)
                            {
                                Current_Index = 0;
                            }
                            gameObject.GetComponent<SpriteRenderer>().sprite = player[Current_Index];
                            Current_Index += 1;
                        }
                        if (((Input.GetKey("down")) && !(Input.GetKey("left"))) && !(Input.GetKey("right")))
                        {
                            if ((Current_Index <= 18) || (Current_Index >= 26))
                            {
                                Current_Index = 18;
                            }
                            gameObject.GetComponent<SpriteRenderer>().sprite = player[Current_Index];
                            Current_Index += 1;
                        }
                        if (((Input.GetKey("left")) && !(Input.GetKey("up"))) && !(Input.GetKey("down")))
                        {
                            if ((Current_Index <= 9) || (Current_Index >= 17))
                            {
                                Current_Index = 9;
                            }
                            gameObject.GetComponent<SpriteRenderer>().sprite = player[Current_Index];
                            Current_Index += 1;

                        }
                        if (((Input.GetKey("right")) && !(Input.GetKey("up"))) && !(Input.GetKey("down")))
                        {
                            if ((Current_Index <= 27) || (Current_Index >= 35))
                            {
                                Current_Index = 27;
                            }
                            gameObject.GetComponent<SpriteRenderer>().sprite = player[Current_Index];
                            Current_Index += 1;
                        }
                        if ((Input.GetKey("right")) && Input.GetKey("up"))
                        {
                            if ((Current_Index <= 27) || (Current_Index >= 35))
                            {
                                Current_Index = 27;
                            }
                            gameObject.GetComponent<SpriteRenderer>().sprite = player[Current_Index];
                            Current_Index += 1;
                        }
                        if ((Input.GetKey("right")) && Input.GetKey("down"))
                        {
                            if ((Current_Index <= 27) || (Current_Index >= 35))
                            {
                                Current_Index = 27;
                            }
                            gameObject.GetComponent<SpriteRenderer>().sprite = player[Current_Index];
                            Current_Index += 1;
                        }
                        if ((Input.GetKey("left")) && Input.GetKey("up"))
                        {
                            if ((Current_Index <= 9) || (Current_Index >= 17))
                            {
                                Current_Index = 9;
                            }
                            gameObject.GetComponent<SpriteRenderer>().sprite = player[Current_Index];
                            Current_Index += 1;
                        }
                        if ((Input.GetKey("left")) && Input.GetKey("down"))
                        {
                            if ((Current_Index <= 9) || (Current_Index >= 17))
                            {
                                Current_Index = 9;
                            }
                            gameObject.GetComponent<SpriteRenderer>().sprite = player[Current_Index];
                            Current_Index += 1;
                        }
                        frames = 0;
                    }
                }
                if (hurt == 1)
                {
                    if (flash < 5)
                    {
                        gameObject.GetComponent<SpriteRenderer>().material.color = new Color(normal.r, normal.g, normal.b, 1);
                        flash += 1;

                    }
                    if ((flash >= 5) && (flash < 10))
                    {
                        gameObject.GetComponent<SpriteRenderer>().material.color = new Color(normal.r, normal.g, normal.b, 0.05f);
                        flash += 1;
                    }
                    if (flash >= 10)
                    {
                        flash = 0;
                    }
                }
                if (hurt == 0)
                {
                    gameObject.GetComponent<SpriteRenderer>().material.color = new Color(normal.r, normal.g, normal.b, 1);
                }
                frames += Time.deltaTime;
            }
            if (game_over == 1)
            {

                if (frames >= (0.05 / speed_variable))
                {
                    if (Current_Index <= 41)
                    {
                        if (Current_Index <= 36)
                        {
                            Current_Index = 36;
                        }
                        gameObject.GetComponent<SpriteRenderer>().sprite = player[Current_Index];
                        Current_Index += 1;
                    }
                    frames = 0;
                }
                frames += Time.deltaTime;
            }
        }
        if (game_win == 1)
        {
            if (fade_speed < 1)
            {
            gameObject.GetComponent<SpriteRenderer>().material.color = new Color(normal.r, normal.g, normal.b, Fade -= 0.01f);
                fade_speed += Time.deltaTime;
            }

        }
    }
}
