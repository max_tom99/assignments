﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinTexture : MonoBehaviour
{
    public Sprite[] player;
    private int Current_Index = 0;
    public float frames = 0;
    private void Start()
    {
        player = Resources.LoadAll<Sprite>("Art/coin_gold");
    }
    // Update is called once per frame
    void Update()
    {
        if (frames >= 0.05)
        {
            if (Current_Index > 7)
            {
                Current_Index = 0;
            }
            gameObject.GetComponent<SpriteRenderer>().sprite = player[Current_Index];
            Current_Index += 1;
            frames = 0;
        }
        frames += Time.deltaTime;
    }
}
