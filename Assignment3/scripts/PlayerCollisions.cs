﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.SocialPlatforms.Impl;

public class PlayerCollisions : MonoBehaviour
{
    public int game_over = 0;
    public int door_open = 0;
    public int health = 100;
    public int coins = 0;
    public float immunity = 0;
    public float timer;
    public int hurt = 0;
    public GameObject player
    {
        get
        {
            return p.gameObject;
        }
    }
    public GameObject orc
    {
        get
        {
            return ot.gameObject;
        }
    }
    public GameObject orc2
    {
        get
        {
            return ot2.gameObject;
        }
    }
    public GameObject skeleton
    {
        get
        {
            return sk.gameObject;
        }
    }
    public GameObject skeleton2
    {
        get
        {
            return sk2.gameObject;
        }
    }
    public GameObject orc_king
    {
        get
        {
            return okt.gameObject;
        }
    }
    public GameObject coin
    {
        get
        {
            return ct.gameObject;
        }
    }
    public GameObject coin2
    {
        get
        {
            return ct2.gameObject;
        }
    }
    public GameObject coin3
    {
        get
        {
            return ct3.gameObject;
        }
    }
    public GameObject exit
    {
        get
        {
            return E.gameObject;
        }
    }
    private PlayerTexture p;
    private OrcTextures ot;
    private OrcTextures2 ot2;
    private SkeletonTexture2 sk;
    private SkeletonTexture sk2;
    private OrcKingTextures okt;
    private CoinTexture ct;
    private CoinTexture2 ct2;
    private CoinTexture3 ct3;
    private ExitDoorTexture E;

    private void Awake()
    {
        p = gameObject.GetComponent<PlayerTexture>();
        ot = GameObject.Find("Orc").GetComponent<OrcTextures>();
        ot2 = GameObject.Find("Orc2").GetComponent<OrcTextures2>();
        sk = GameObject.Find("Skeleton").GetComponent<SkeletonTexture2>();
        sk2 = GameObject.Find("Skeleton2").GetComponent<SkeletonTexture>();
        okt = GameObject.Find("OrcKing").GetComponent<OrcKingTextures>();
        ct = GameObject.Find("Coin").GetComponent<CoinTexture>();
        ct2 = GameObject.Find("Coin2").GetComponent<CoinTexture2>();
        ct3 = GameObject.Find("Coin3").GetComponent<CoinTexture3>();
        E = GameObject.Find("Exit").GetComponent<ExitDoorTexture>();

    }

    // Update is called once per frame
    void Update()
    {
        if (orc.GetComponent<SpriteRenderer>().bounds.Intersects(player.GetComponent<SpriteRenderer>().bounds))
        {
            if (immunity == 0)
            {
                health -= 25;
                immunity = 1;
                timer = 0;
                hurt = 1;
            }
        }
        if (orc2.GetComponent<SpriteRenderer>().bounds.Intersects(player.GetComponent<SpriteRenderer>().bounds))
        {
            if (immunity == 0)
            {
                health -= 25;
                immunity = 1;
                timer = 0;
                hurt = 1;
            }
        }
        if (skeleton.GetComponent<SpriteRenderer>().bounds.Intersects(player.GetComponent<SpriteRenderer>().bounds))
        {
            if (immunity == 0)
            {
                health -= 10;
                immunity = 1;
                timer = 0;
                hurt = 1;
            }
        }
        if (skeleton2.GetComponent<SpriteRenderer>().bounds.Intersects(player.GetComponent<SpriteRenderer>().bounds))
        {
            if (immunity == 0)
            {
                health -= 10;
                immunity = 1;
                timer = 0;
                hurt = 1;
            }
        }
        if (orc_king.GetComponent<SpriteRenderer>().bounds.Intersects(player.GetComponent<SpriteRenderer>().bounds))
        {
            if (immunity == 0)
            {
                health -= 50;
                immunity = 1;
                timer = 0;
                hurt = 1;
            }
        }
        if (health <= 0)
        {
            game_over = 1;
        }
        if (GameObject.Find("Coin").GetComponent<CoinTexture>() != null)
        {
            if (coin.GetComponent<SpriteRenderer>().bounds.Intersects(player.GetComponent<SpriteRenderer>().bounds))
            {
                Destroy(GameObject.Find("Coin").GetComponent<CoinTexture>());
                GameObject.Find("Coin").GetComponent<SpriteRenderer>().sprite = null;
                coins += 1;
            }
        }
        if (GameObject.Find("Coin2").GetComponent<CoinTexture2>() != null)
        {
            if (coin2.GetComponent<SpriteRenderer>().bounds.Intersects(player.GetComponent<SpriteRenderer>().bounds))
            {
                Destroy(GameObject.Find("Coin2").GetComponent<CoinTexture2>());
                GameObject.Find("Coin2").GetComponent<SpriteRenderer>().sprite = null;
                coins += 1;
            }
        }
        if (GameObject.Find("Coin3").GetComponent<CoinTexture3>() != null)
        {
            if (coin3.GetComponent<SpriteRenderer>().bounds.Intersects(player.GetComponent<SpriteRenderer>().bounds))
            {
                Destroy(GameObject.Find("Coin3").GetComponent<CoinTexture3>());
                GameObject.Find("Coin3").GetComponent<SpriteRenderer>().sprite = null;
                coins += 1;
            }
        }
        if (exit.GetComponent<SpriteRenderer>().bounds.Intersects(player.GetComponent<SpriteRenderer>().bounds))
        {
            if (Input.GetKey("up"))
            {

                door_open = 1;
            }
        }
        if (timer > 1)
        {
            immunity = 0;
            hurt = 0;
        }
        timer += Time.deltaTime;
    }
}
