﻿using System;

namespace Assignment2
{
    class Program
    {
        static void Main(string[] args)
        {
            int game_over = 0;
            Console.WriteLine("Welcome to 'The Game'!");
            int[] gameboard = new int[26] {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
            var rand = new Random();
            var hazard1 = 0;
            var hazard2 = 0; 
            var treasure1= 0; 
            var treasure2= 0;
            var exit= 0;
            var laser = 0;
            int direction = 0;
            //creating the unique parts of the game board and assigning them, while also checking to make sure no two parts are
            //on the same tile of the grid.
            var player = rand.Next(0, 25);
            gameboard[player] = 1;
            do
            {
                hazard1 = rand.Next(0, 25);
            } while (player == hazard1);
            do
            {
                hazard2 = rand.Next(0, 25);
            } while ((player == hazard2) || (hazard1 == hazard2));
            do
            {
                treasure1 = rand.Next(0, 25);
            } while ((player == treasure1) || (hazard1 == treasure1) || (hazard2 == treasure1));
            do
            {
                treasure2 = rand.Next(0, 25);
            } while ((player == treasure2) || (hazard1 == treasure2) || (hazard2 == treasure2) || (treasure1 == treasure2));
            do
            {
                exit = rand.Next(0, 25);
            } while ((player == exit) || (hazard1 == exit) || (hazard2 == exit) || (treasure1 == exit) || (treasure2 == exit));
            do
            {
                laser = rand.Next(0, 25);
            } while ((player == laser) || (hazard1 == laser) || (hazard2 == laser) || (treasure1 == laser) || (treasure2 == laser) || (exit == laser));
            //prints the game board for the first time
            gameboard[player] = 1;
            gameboard[hazard1] = 2;
            gameboard[hazard2] = 2;
            gameboard[treasure1] = 3;
            gameboard[treasure2] = 3;
            gameboard[exit] = 4;
            gameboard[laser] = 5;
            int tile_number = 0;
            while (tile_number < 25)
            {
                int skipline = 0;
                while (skipline < 5)
                {
                    if (gameboard[tile_number] == 1)
                    {
                        Console.Write("P");
                    }
                    else if (gameboard[tile_number] == 2)
                    {
                        Console.Write("H");
                    }
                    else if (gameboard[tile_number] == 3)
                    {
                        Console.Write("T");
                    }
                    else if (gameboard[tile_number] == 4)
                    {
                        Console.Write("X");
                    }
                    else if (gameboard[tile_number] == 5)
                    {
                        Console.Write("\u26A1");
                    }
                    else
                    {
                        Console.Write("_");
                    }
                    Console.Write(" ");
                    skipline += 1;
                    tile_number += 1;
                }
                Console.WriteLine("");
            }
            int treasure_gained = 0;
            while (game_over == 0)
            {
                    //player is on the right wall.
                    if (player == 4 || (player == 9  || (player == 14 || (player == 19 || (player == 24))))){
                    if (player == (hazard1 + 1) || (player == (hazard1 + 5) || (player == (hazard1 - 5))))
                    {
                        Console.WriteLine("You are next to a hazard. Watch out!");
                    }
                    if (player == (hazard2 + 1) || (player == (hazard2 + 5) || (player == (hazard2 - 5))))
                    {
                        Console.WriteLine("You are next to a hazard. Watch out!");
                    }
                    if (player == (laser + 1) || (player == (laser + 5) || (player == (laser- 5))))
                    {
                        Console.WriteLine("Watch out for the laser beam!");
                    }
                    if (player == (treasure1 + 1) || (player == (treasure1 + 5) || (player == (treasure1 - 5))))
                    {
                        Console.WriteLine("You are next to a treasure");
                    }
                    if (player == (treasure2 + 1) || (player == (treasure2 + 5) || (player == (treasure2 - 5))))
                    {
                        Console.WriteLine("You are next to a treasure");
                    }
                    if (player == (exit + 1) || (player == (exit + 5) || (player == (exit - 5))))
                    {
                        Console.WriteLine("You are next to the exit");
                    }
                }
                //player is on the left wall.
                else if (player == 0 || (player == 5 || (player == 10 || (player == 15 || (player == 20)))))
                {
                    if (player == (hazard1 - 1) || (player == (hazard1 + 5) || (player == (hazard1 - 5))))
                    {
                        Console.WriteLine("You are next to a hazard. Watch out!");
                    }
                    if (player == (hazard2 - 1) || (player == (hazard2 + 5) || (player == (hazard2 - 5))))
                    {
                        Console.WriteLine("You are next to a hazard. Watch out!");
                    }
                    if (player == (laser - 1) || (player == (laser + 5) || (player == (laser - 5))))
                    {
                        Console.WriteLine("Watch out for the laser beam!");
                    }
                    if (player == (treasure1 - 1) || (player == (treasure1 + 5) || (player == (treasure1 - 5))))
                    {
                        Console.WriteLine("You are next to a treasure");
                    }
                    if (player == (treasure2 - 1) || (player == (treasure2 + 5) || (player == (treasure2 - 5))))
                    {
                        Console.WriteLine("You are next to a treasure");
                    }
                    if (player == (exit - 1) || (player == (exit + 5) || (player == (exit - 5))))
                    {
                        Console.WriteLine("You are next to the exit");
                    }
                }
                //player is anywhere else.
                else 
                {
                    if (player == (hazard1 + 1) || (player == (hazard1 - 1) || (player == (hazard1 + 5) || (player == (hazard1 - 5)))))
                    {
                        Console.WriteLine("You are next to a hazard. Watch out!");
                    }
                    if (player == (hazard2 + 1) || (player == (hazard2 - 1) || (player == (hazard2 + 5) || (player == (hazard2 - 5)))))
                    {
                        Console.WriteLine("You are next to a hazard. Watch out!");
                    }
                    if (player == (laser + 1) || (player == (laser - 1) || (player == (laser + 5) || (player == (laser - 5)))))
                    {
                        Console.WriteLine("Watch out for the laser beam!");
                    }
                    if (player == (treasure1 + 1) || (player == (treasure1 - 1) || (player == (treasure1 + 5) || (player == (treasure1 - 5)))))
                    {
                        Console.WriteLine("You are next to a treasure");
                    }
                    if (player == (treasure2 + 1) || (player == (treasure2 - 1) || (player == (treasure2 + 5) || (player == (treasure2 - 5)))))
                    {
                        Console.WriteLine("You are next to a treasure");
                    }
                    if (player == (exit + 1) || (player == (exit - 1) || (player == (exit + 5) || (player == (exit - 5)))))
                    {
                        Console.WriteLine("You are next to the exit");
                    }
                }
                if ((player == hazard1) || (player == hazard2))
                {
                    Console.WriteLine("You Lose, you fell in a trap. Bad luck.");
                    game_over = 1;
                }
                if (player == laser)
                {
                    Console.WriteLine("You were cut in two by a laser beam. Try again next time.");
                    game_over = 1;
                }
                if (player == treasure1)
                {
                    Console.WriteLine("You picked up a treasure!");
                    treasure_gained += 1;
                    gameboard[treasure1] = 0;
                    treasure1 = 25;
                }
                if (player == treasure2)
                {
                    Console.WriteLine("You picked up a treasure!");
                    treasure_gained += 1;
                    gameboard[treasure2] = 0;
                    treasure2 = 25;
                }
                if (player == exit)
                {
                    Console.WriteLine($"You are standing on the exit, make sure you have all the treasures before you leave!");
                }
                //character input.
                Console.WriteLine("Which action would you like to perform: E(ast), W(est), N(orth), S(outh), X(exit), G(ameboard):");
                string character_choice;
                character_choice = Console.ReadLine();
                character_choice = character_choice.ToLower();
                //prints the game board. this is in its own seperate loop so that printing the
                //game board doesnt take up a turn.
                if (character_choice == "g")
                {
                    gameboard[player] = 1;
                    gameboard[hazard1] = 2;
                    gameboard[hazard2] = 2;
                    gameboard[treasure1] = 3;
                    gameboard[treasure2] = 3;
                    gameboard[exit] = 4;
                    gameboard[laser] = 5;
                    tile_number = 0;
                    while (tile_number < 25)
                    {
                        int skipline = 0;
                        while (skipline < 5)
                        {
                            if (gameboard[tile_number] == 1)
                            {
                                Console.Write("P");
                            }
                            else if (gameboard[tile_number] == 2)
                            {
                                Console.Write("H");
                            }
                            else if (gameboard[tile_number] == 3)
                            {
                                Console.Write("T");
                            }
                            else if (gameboard[tile_number] == 4)
                            {
                                Console.Write("X");
                            }
                            else if (gameboard[tile_number] == 5)
                            {
                                Console.Write("\u26A1");
                            }
                            else
                            {
                                Console.Write("_");
                            }
                            Console.Write(" ");
                            skipline += 1;
                            tile_number += 1;
                        }
                        Console.WriteLine("");
                    }
                }
                //any other imput other than 'g'.
                else
                {
                    if (character_choice == "e")
                    {
                        if (((((player != 4) && (player != 9)) && (player != 14)) && (player != 19)) && (player != 24))
                        {
                            gameboard[player] = 0;
                            player += 1;
                        }
                        else
                        {
                            Console.WriteLine("You cannot move that way, try to stay on the board.");
                        }
                    }
                    else if (character_choice == "w")
                    {
                        if (((((player != 0) && (player != 5)) && (player != 10)) && (player != 15)) && (player != 20))
                        {
                            gameboard[player] = 0;
                            player -= 1;
                        }
                        else
                        {
                            Console.WriteLine("You cannot move that way, try to stay on the board.");
                        }
                    }
                    else if (character_choice == "n")
                    {
                        if (player > 4)
                        {
                            gameboard[player] = 0;
                            player -= 5;
                        }
                        else
                        {
                            Console.WriteLine("You cannot move that way, try to stay on the board.");
                        }
                    }
                    else if (character_choice == "s")
                    {
                        if (player < 20)
                        {
                            gameboard[player] = 0;
                            player += 5;
                        }
                        else
                        {
                            Console.WriteLine("You cannot move that way, try to stay on the board.");
                        }
                    }
                    else if (character_choice == "x")
                    {
                        if (gameboard[player] == gameboard[exit])
                        {
                            if (treasure_gained > 0)
                            {
                                Console.WriteLine($"Congratulations, you made it out, and you took {treasure_gained} treasure with you.");
                                game_over = 1;
                            }
                            else
                            {
                                Console.WriteLine($"Congratulations, you made it out, but you forgot the treasure!");
                                game_over = 1;
                            }
                        }
                        else
                        {
                            Console.WriteLine("Don't get ahead of yourself, you aren't at the exit yet.");
                        }
                    }
                    else
                    {
                        Console.WriteLine("Oops, unexpected input, please try again.");
                    }
                    //Updates the position of the laser, pacing it up and down the game board by 1 tile every turn.
                    if (laser > 19)
                    {
                        gameboard[laser] = 0;
                        direction = 1;
                        laser -= 5;
                    }
                    else if (laser < 5)
                    {
                        gameboard[laser] = 0;
                        direction = 0;
                        laser += 5;
                    }
                    else
                    {
                        if (direction == 0)
                        {
                            gameboard[laser] = 0;
                            laser += 5;
                        }
                        else if (direction == 1)
                        {
                            gameboard[laser] = 0;
                            laser -= 5;
                        }
                    }
                }
            }
        }
    }
}
